package fr.eloxfire.main;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class Styles {
	
	//APP COLORS
	public static Color black = Color.decode("#000000");
	public static Color white = Color.decode("#FFFFFF");
	public static Color dark_grey = Color.decode("#606060");
	public static Color blue_gray = Color.decode("#778CA3");
	public static Color green = Color.decode("#20bf6b");
//	private static Color purple = Color.decode("#8854D0");
	
	public static void setButtonStyles(JButton button, Color backgroundColor, Color foregroudColor) {
		button.setBorderPainted(false);
		button.setBackground(backgroundColor);
		button.setForeground(foregroudColor);
		button.setFocusPainted(false);
	}
	
	public static void setTitleStyle(JLabel label, Color c) {
		label.setFont(new Font("Calibri", Font.BOLD, 30));
		label.setForeground(c);
	}
	
	public static void setTextStyle(JLabel label) {
		label.setFont(new Font("Calibri", 0, 15));
		label.setForeground(white);
	}
	
	public static void setInputStyles(JTextField input) {
		input.setBorder(null);
	}
	
	public static void setSelectStyles(JComboBox<?> select) {
		select.setBorder(new EmptyBorder(0, 0,0,0));
		select.setBackground(white);
	}
	
//	public static void setEqualsTextStyle(JLabel label) {
//		label.setFont(new Font("Calibri", 0, 15));
//		label.setForeground(white);
//	}

}

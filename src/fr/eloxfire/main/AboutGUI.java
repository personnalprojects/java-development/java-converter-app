package fr.eloxfire.main;

import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class AboutGUI implements ActionListener{
	
	private static JFrame aboutFrame;
	private static int ABOUT_WIDTH = 360;
	private static int ABOUT_HEIGHT = 203;
	
	private static JLabel title, subtitle, licenseLink, appVersion;
	private static JButton closeButton;
	
	public AboutGUI() {
		URL url = Main.class.getResource("/Logo.png");
		ImageIcon img = new ImageIcon(url);
		aboutFrame = new JFrame("� propos de Multivertt");
		aboutFrame.setIconImage(img.getImage());
        aboutFrame.setResizable(false);
        aboutFrame.setLayout(null);
        aboutFrame.setSize(ABOUT_WIDTH, ABOUT_HEIGHT);
        aboutFrame.setLocationRelativeTo(Main.frame);
        aboutFrame.getContentPane().setBackground(Styles.dark_grey);
         
        title = new JLabel("� propos de Multivertt");
		subtitle = new JLabel("Copyright � Enzo Avagliano | 2022");
		licenseLink = new JLabel("Logiciel fournis sous license " + Utils.getAppLicense());
		appVersion = new JLabel("Version : " + Utils.getAppVersion());
		closeButton = new JButton("Fermer");
		
		title.setBounds(ABOUT_WIDTH/2 - 80, 10, 200, 20);
		subtitle.setBounds(ABOUT_WIDTH/2 - 115, 50, 230, 20);
		licenseLink.setBounds(ABOUT_WIDTH/2 - 125, 70, 250, 20);
		appVersion.setBounds(ABOUT_WIDTH/2 - 42, 90, 90, 20);
		closeButton.setBounds((ABOUT_WIDTH / 2) - 40, ABOUT_HEIGHT - 80, 80, 25);
		
		Styles.setTextStyle(title);
		Styles.setTextStyle(subtitle);
		Styles.setTextStyle(licenseLink);
		Styles.setTextStyle(appVersion);
		
		closeButton.addActionListener(this);
		Styles.setButtonStyles(closeButton, Styles.blue_gray, Styles.white);
		
		licenseLink.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		licenseLink.addMouseListener(new MouseAdapter() {
			@Override
		    public void mouseClicked(MouseEvent e) {
				try {
			        Desktop.getDesktop().browse(new URI("https://choosealicense.com/licenses/gpl-3.0/"));
			    } catch (IOException | URISyntaxException e1) {
			        e1.printStackTrace();
			    }
		    }
		});
        
        aboutFrame.add(title);
        aboutFrame.add(subtitle);
        aboutFrame.add(licenseLink);		
        aboutFrame.add(appVersion);
        aboutFrame.add(closeButton); 
        aboutFrame.setVisible(true);   
	}
	
	public static void makeAboutGUI() {
		new AboutGUI();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Fermer":
			System.out.println(e.getActionCommand());
			aboutFrame.dispose();
			break;
		}
	}
}

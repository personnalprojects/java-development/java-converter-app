package fr.eloxfire.main;

public class Utils {

	public static String fmt(double d)
	{
	    if(d == (long) d)
	        return String.format("%d",(long)d);
	    else
	        return String.format("%s",d);
	}
	
	public static String getAppVersion() {
		return "1.1.0";
	}
	
	public static String getAppLicense() {
		return "GNU GPLv3";
	}
}

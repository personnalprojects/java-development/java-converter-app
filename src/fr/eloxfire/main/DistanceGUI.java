package fr.eloxfire.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.util.Arrays;
import java.util.Iterator;

public class DistanceGUI implements ActionListener{
	
	private static String units[] = new String[] {"Yottam�tre",
			"Zettam�tre", "Exam�tre", "P�tam�tre", "T�ram�tre", "Gigam�tre", "M�gam�tre", "Kilom�tre",
			"Hectom�tre", "D�cam�tre", "M�tre", "D�cim�tre", "Centim�tre", "Millim�tre", "Microm�tre",
			"Nanom�tre", "Picom�tre", "Femtom�tre", "Attom�tre", "Zeptom�tre", "Yoctom�tre"};
	
	
	private static JLabel distanceGuiTitle, description, equalsText, result;
	private static JButton distanceGuiBackButton, calculateResultButton;
	private static JTextField startValue;
	private static JComboBox<String> startUnitSelector;
	private static JComboBox<String> endUnitSelector;
	
	
	private static String selectedStartUnit, selectedEndUnit;
	private static double selectedStartValue, resultValue;
	
	public DistanceGUI(JFrame frame) {
		frame.getContentPane().removeAll();
		frame.repaint();
		
		distanceGuiTitle = new JLabel("Convertisseur de distances");
		description = new JLabel("<html><body>Entrez une valeur de d�part, s�lectionnez son unit�. Choisissez l'unit� dans laquelle vous souhaitez convertir votre valeur initiale, l'application s'occupe du reste !</body></html>");
		distanceGuiBackButton = new JButton("Retour");
		calculateResultButton = new JButton("Convertir");
		startUnitSelector = new JComboBox<>(units);
		endUnitSelector = new JComboBox<>(units);
		startValue = new JTextField("1000");
		equalsText = new JLabel("=");
		result = new JLabel("0");
		
		distanceGuiTitle.setBounds(30, 20, 500, 50);
		description.setBounds(30, 60, 500, 60);
		startValue.setBounds(30, Main.WINDOW_HEIGHT /2 - 60, 80, 30);
		startUnitSelector.setBounds(115, Main.WINDOW_HEIGHT /2 - 60, 120, 30);
		endUnitSelector.setBounds(400, Main.WINDOW_HEIGHT /2 - 60, 120, 30);
		distanceGuiBackButton.setBounds(30, Main.WINDOW_HEIGHT - 100, 100, 40);
		calculateResultButton.setBounds(30, Main.WINDOW_HEIGHT /2, 120, 30);
		equalsText.setBounds(250, Main.WINDOW_HEIGHT /2 - 55, 120, 30);
		result.setBounds(300, Main.WINDOW_HEIGHT /2 - 55, 120, 30);
		
		startUnitSelector.setSelectedItem("M�tre");
		endUnitSelector.setSelectedItem("Kilom�tre");
		
		distanceGuiBackButton.addActionListener(this);
		calculateResultButton.addActionListener(this);
		
		Styles.setTitleStyle(distanceGuiTitle, Styles.white);
		Styles.setTextStyle(description);
		Styles.setButtonStyles(distanceGuiBackButton, Styles.blue_gray, Styles.white);
		Styles.setButtonStyles(calculateResultButton, Styles.green, Styles.white);
		Styles.setTitleStyle(equalsText, Styles.white);
		Styles.setInputStyles(startValue);
		Styles.setTextStyle(result);
		Styles.setSelectStyles(startUnitSelector);
		Styles.setSelectStyles(endUnitSelector);
		
		frame.add(equalsText);
		frame.add(distanceGuiTitle);
		frame.add(description);
		frame.add(distanceGuiBackButton);
		frame.add(calculateResultButton);
		frame.add(startUnitSelector);
		frame.add(endUnitSelector);
		frame.add(startValue);
		frame.add(result);
	}
	
	public static void makeDistanceGUI(JFrame frame) {
		new DistanceGUI(frame);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Retour":
			System.out.println(e.getActionCommand());
			MainGUI.makeMainGUI(Main.frame);
			break;
		case "Convertir":
			performConvertion();
			break;
		}
	}
	
	private static void performConvertion() {
		int startUnitIndex, endUnitIndex, unitsGap;
		String order;
		
		selectedStartUnit = String.valueOf(startUnitSelector.getSelectedItem());
		selectedEndUnit = String.valueOf(endUnitSelector.getSelectedItem());
		selectedStartValue = Float.parseFloat(String.valueOf(startValue.getText()));
		startUnitIndex = Arrays.asList(units).indexOf(selectedStartUnit);
		endUnitIndex = Arrays.asList(units).indexOf(selectedEndUnit);
		
		System.out.println("Selected start unit : " + selectedStartUnit + "\tSelected start value : " + selectedStartValue);
		System.out.println("Selected end unit : " + selectedEndUnit);
		System.out.println("Start unit index :" + startUnitIndex + "\tEnd unit index : " + endUnitIndex);

		if(startUnitIndex > endUnitIndex) {
			unitsGap = startUnitIndex - endUnitIndex;
			order = "NEGATIVE";
			System.out.println("Unit gap :" + unitsGap + "\tOrder : " + order);
		}else {
			unitsGap = endUnitIndex - startUnitIndex;
			order = "POSITIVE";
			System.out.println("Unit gap :" + unitsGap + "\tOrder : " + order);
		}

		resultValue = selectedStartValue;
		for (int i = 0; i < unitsGap; i++) {
			if(order == "POSITIVE") {
				resultValue = resultValue * 10;
			}else {
				resultValue = resultValue / 10;
			}
		}
		
		System.out.println("Result : " + resultValue);
		result.setText(String.valueOf(resultValue));
		result.setBounds(300, Main.WINDOW_HEIGHT /2 - 55, result.getPreferredSize().width, 30);
		int endResultPos = result.getBounds().x + result.getBounds().width;
		System.out.println(result.getPreferredSize().width);
		endUnitSelector.setBounds(endResultPos + 10, Main.WINDOW_HEIGHT /2 - 60, 120, 30);
	}
}

package fr.eloxfire.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class TimeGUI implements ActionListener{
	
	private static String units[] = new String[] {"Ann�e", "Mois", "Semaine", "Jour", "Heure", "Minute", "Seconde", "Milliseconde"};
	
	private static JLabel timeGuiTitle, description, equalsText, result;
	private static JButton timeGuiBackButton, calculateResultButton;
	private static JTextField startValue;
	private static JComboBox<String> startUnitSelector, endUnitSelector;
	
	public TimeGUI(JFrame frame) {
		frame.getContentPane().removeAll();
		frame.repaint();
		
		timeGuiTitle = new JLabel("Convertisseur de dur�es");
		description = new JLabel("<html><body>Entrez une valeur de d�part, s�lectionnez son unit�. Choisissez l'unit� dans laquelle vous souhaitez convertir votre valeur initiale, l'application s'occupe du reste !</body></html>");
		timeGuiBackButton = new JButton("Retour");
		calculateResultButton = new JButton("Convertir");
		startUnitSelector = new JComboBox<>(units);
		endUnitSelector = new JComboBox<>(units);
		startValue = new JTextField("60");
		equalsText = new JLabel("=");
		result = new JLabel("1");
		
		timeGuiTitle.setBounds(30, 20, 500, 50);
		description.setBounds(30, 60, 500, 60);
		startValue.setBounds(30, Main.WINDOW_HEIGHT /2 - 60, 80, 30);
		startUnitSelector.setBounds(115, Main.WINDOW_HEIGHT /2 - 60, 120, 30);
		endUnitSelector.setBounds(350, Main.WINDOW_HEIGHT /2 - 60, 120, 30);
		result.setBounds(310, Main.WINDOW_HEIGHT /2 - 55, 120, 30);
		timeGuiBackButton.setBounds(30, Main.WINDOW_HEIGHT - 100, 100, 40);
		equalsText.setBounds(250, Main.WINDOW_HEIGHT /2 - 55, 120, 30);
		calculateResultButton.setBounds(30, Main.WINDOW_HEIGHT /2, 120, 30);
		
		startUnitSelector.setSelectedItem("Minute");
		endUnitSelector.setSelectedItem("Heure");
		
		timeGuiBackButton.addActionListener(this);
		calculateResultButton.addActionListener(this);
		
		Styles.setTitleStyle(timeGuiTitle, Styles.white);
		Styles.setTextStyle(description);
		Styles.setButtonStyles(timeGuiBackButton, Styles.blue_gray, Styles.white);
		Styles.setButtonStyles(calculateResultButton, Styles.green, Styles.white);
		Styles.setTitleStyle(equalsText, Styles.white);
		Styles.setSelectStyles(startUnitSelector);
		Styles.setSelectStyles(endUnitSelector);
		Styles.setTextStyle(result);
		
		frame.add(equalsText);
		frame.add(timeGuiTitle);
		frame.add(calculateResultButton);
		frame.add(description);
		frame.add(timeGuiBackButton);
		frame.add(startUnitSelector);
		frame.add(endUnitSelector);
		frame.add(startValue);
		frame.add(result);
	}
	
	public static void makeTimeGUI(JFrame frame) {
		new TimeGUI(frame);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Retour":
			System.out.println(e.getActionCommand());
			MainGUI.makeMainGUI(Main.frame);
			break;
		case "Convertir":
			System.out.println(e.getActionCommand());
			performConvertion();
			break;
		}
	}
	
	private static void performConvertion() {
		float selectedStartValue = Float.parseFloat(String.valueOf(startValue.getText()));
	}
}
